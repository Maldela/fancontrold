use fancontrold::*;

use std::{
    env,
    ffi::{OsStr, OsString},
    sync::Arc,
    time::Duration,
};

use ::dbus::channel::Sender;
use futures::lock::Mutex;
use getopts::{Matches, Options};
use log::*;
use syslog::Facility;

const UPDATE_INTERVAL: Duration = Duration::from_millis(1000);
const DEFAULT_CONFIG_PATH: &str = "/etc/fancontrold.json";

fn print_usage(program: &OsStr, opts: Options) {
    let brief = format!("Usage: {} [options]", program.to_string_lossy());
    print!("{}", opts.usage(&brief));
}

fn init_syslog(level_filter: LevelFilter) {
    if let Err(e) = syslog::init(Facility::LOG_DAEMON, level_filter, None) {
        panic!("Error initializing syslog: {}", e);
    }
}

fn arg_options() -> Options {
    let mut opts = Options::new();
    opts.optflag("h", "help", "display this help message");
    opts.optopt(
        "v",
        "verbose",
        "verbosity, 1 for error up to 5 for trace",
        "1-5",
    );
    opts.optopt(
        "c",
        "config",
        &format!("set config file (default: {DEFAULT_CONFIG_PATH})"),
        "PATH",
    );
    opts.optflag("i", "inspect", "only inspect config for errors");

    opts
}

fn get_level_filter(matches: &Matches) -> LevelFilter {
    match matches.opt_str("v") {
        Some(v) => match v.as_str() {
            "1" => LevelFilter::Error,
            "2" => LevelFilter::Warn,
            "3" => LevelFilter::Info,
            "4" => LevelFilter::Debug,
            "5" => LevelFilter::Trace,
            _ => LevelFilter::Warn,
        },
        None => LevelFilter::Warn,
    }
}

#[tokio::main]
async fn main() {
    // parse arguments
    let args: Vec<OsString> = env::args_os().collect();
    let program = &args[0];
    let opts = arg_options();

    let matches = opts
        .parse(&args[1..])
        .unwrap_or_else(|e| panic!("Error parsing arguments: {}", e));

    if matches.opt_present("h") {
        print_usage(program, opts);
        return;
    }

    let level_filter = get_level_filter(&matches);

    init_syslog(level_filter);

    let config_path = matches
        .opt_str("c")
        .unwrap_or_else(|| String::from(DEFAULT_CONFIG_PATH));

    let fancontrold = match Fancontrold::new().await {
        Ok(fc) => Arc::new(Mutex::new(fc)),
        Err(e) => {
            error!("Error parsing hwmons: {}", e);
            return;
        }
    };

    if matches.opt_present("i") {
        match fancontrold
            .lock_owned()
            .await
            .load_config(&config_path)
            .await
        {
            Ok(_) => println!("Config is valid"),
            Err(e) => eprintln!("{}", e),
        }

        return;
    }

    // d-bus
    let (_dbus_handle, dbus_conn) = match dbus::create_connection() {
        Ok((res, conn)) => {
            let _handle = tokio::spawn(async {
                let err = res.await;
                error!("Lost connection to D-Bus: {}", err);
            });

            if let Err(e) = dbus::init_connection(conn.clone(), fancontrold.clone()).await {
                error!("D-Bus error: {}", e);
                return;
            }

            (_handle, conn)
        }
        Err(e) => {
            error!("D-Bus error: {}", e);
            return;
        }
    };

    let fancontrold_clone = fancontrold.clone();

    {
        match mulligan::until_ok()
            .stop_after(5)
            .max_delay(Duration::from_secs(3))
            .exponential(Duration::from_secs(1))
            .retry(move || {
                let config_path_clone = config_path.clone();
                let fancontrold = fancontrold_clone.clone();

                async move {
                    fancontrold
                        .lock()
                        .await
                        .load_config(&config_path_clone)
                        .await
                }
            })
            .await
        {
            Ok(_) => {
                let msg =
                    crate::dbus::new_config_signal(fancontrold.lock().await.config().as_string());
                let _ = dbus_conn.send(msg);
            }
            Err(e) => {
                warn!("Error loading config: {}", e);

                let msg = crate::dbus::new_error_signal(e.to_string());
                let _ = dbus_conn.send(msg);
            }
        }
    }

    let mut update_interval = tokio::time::interval(UPDATE_INTERVAL);

    loop {
        update_interval.tick().await;

        debug!("Updating...");

        if let Err(e) = fancontrold.lock().await.update().await {
            error!("Error during update: {}", e);

            let msg = crate::dbus::new_error_signal(e.to_string());
            let _ = dbus_conn.send(msg);
        }
    }
}
