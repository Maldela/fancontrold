use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    result::Result as StdResult,
    sync::mpsc::RecvError,
};

use dbus::Error as DBusError;

pub type Result<T> = StdResult<T, Error>;

#[derive(Debug)]
pub enum Error {
    DBus(DBusError),
    ConnectionNameUnavailable,
    Channel(RecvError),
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Error::DBus(source) => write!(f, "DBus error: {}", source),
            Error::ConnectionNameUnavailable => write!(f, "DBus connection name unavailable."),
            Error::Channel(source) => write!(f, "Error communicating with dbus thread: {}", source),
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            Error::DBus(source) => Some(source),
            Error::ConnectionNameUnavailable => None,
            Error::Channel(source) => Some(source),
        }
    }
}

impl From<DBusError> for Error {
    fn from(source: DBusError) -> Self {
        Error::DBus(source)
    }
}

impl From<RecvError> for Error {
    fn from(source: RecvError) -> Self {
        Error::Channel(source)
    }
}
