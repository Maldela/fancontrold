mod error;

use super::Fancontrold;
use error::Result;

use std::sync::Arc;

use dbus::{
    channel::MatchingReceiver,
    nonblock::{stdintf::org_freedesktop_dbus::RequestNameReply, SyncConnection},
    Message,
};
use dbus_crossroads::{Context, Crossroads, MethodErr};
use dbus_tokio::connection::{new_system_sync, IOResource};
use futures::lock::Mutex;

pub fn dbus_conn_name() -> &'static str {
    option_env!("DBUS_CONNECTION_NAME").unwrap_or("org.kde.fancontrold")
}

pub fn dbus_interface_name() -> &'static str {
    option_env!("DBUS_INTERFACE_NAME").unwrap_or("org.kde.fancontrold")
}

pub fn dbus_object_path() -> &'static str {
    option_env!("DBUS_OBJECT_PATH").unwrap_or("/org/kde/fancontrold")
}

fn signal_new_config(cx: &mut Context, config: impl Into<String>) {
    let new_config_signal_msg = cx.make_signal("NewConfig", (config.into(),));
    cx.push_msg(new_config_signal_msg);
}

pub fn create_connection() -> Result<(IOResource<SyncConnection>, Arc<SyncConnection>)> {
    new_system_sync().map_err(Into::into)
}

pub async fn init_connection(conn: Arc<SyncConnection>, fc: Arc<Mutex<Fancontrold>>) -> Result<()> {
    if RequestNameReply::Exists
        == conn
            .request_name(dbus_conn_name(), false, true, false)
            .await?
    {
        return Err(error::Error::ConnectionNameUnavailable);
    };

    let mut cr = Crossroads::new();

    // Enable async support for the crossroads instance.
    cr.set_async_support(Some((
        conn.clone(),
        Box::new(|x| {
            tokio::spawn(x);
        }),
    )));

    let interface = cr.register(dbus_interface_name(), |b| {
        b.signal::<(String,), _>("NewConfig", ("config",));

        b.signal::<(String,), _>("Error", ("error",));

        b.property("Config")
            .get_async(|mut cx, fc: &mut Arc<Mutex<Fancontrold>>| {
                let fc = fc.clone();

                async move {
                    let reply = fc.lock_owned().await.config().as_string();

                    cx.reply(Ok(reply))
                }
            })
            .emits_changed_false();

        b.property("ConfigPath")
            .get_async(|mut cx, fc: &mut Arc<Mutex<Fancontrold>>| {
                let fc = fc.clone();

                async move {
                    let reply = fc
                        .lock_owned()
                        .await
                        .config_path()
                        .to_str()
                        .map(String::from)
                        .ok_or_else(|| {
                            MethodErr::failed("Config path contains non-unicode characters.")
                        });

                    cx.reply(reply)
                }
            })
            .emits_changed_false();

        b.method_with_cr_async("Load", ("path",), (), |mut cx, cr, (path,): (String,)| {
            let fc = cr
                .data_mut::<Arc<Mutex<Fancontrold>>>(cx.path())
                .map(|d| d.clone())
                .ok_or_else(|| MethodErr::no_path(cx.path()));

            async move {
                match fc {
                    Ok(fc) => {
                        let mut fc = fc.lock_owned().await;

                        let result = fc
                            .load_config(path)
                            .await
                            .map_err(|e| MethodErr::failed(&e));
                        let new_config = fc.config().as_string();
                        signal_new_config(&mut cx, new_config);

                        cx.reply(result)
                    }
                    Err(e) => cx.reply(Err(e)),
                }
            }
        });

        b.method_with_cr_async("Reload", (), (), |mut cx, cr, ()| {
            let fc = cr
                .data_mut::<Arc<Mutex<Fancontrold>>>(cx.path())
                .map(|d| d.clone())
                .ok_or_else(|| MethodErr::no_path(cx.path()));

            async move {
                match fc {
                    Ok(fc) => {
                        let mut fc = fc.lock_owned().await;

                        let result = fc.reload_config().await.map_err(|e| MethodErr::failed(&e));
                        let new_config = fc.config().as_string();
                        signal_new_config(&mut cx, new_config);

                        cx.reply(result)
                    }
                    Err(e) => cx.reply(Err(e)),
                }
            }
        });

        b.method_with_cr_async(
            "Apply",
            ("config",),
            (),
            |mut cx, cr, (config,): (String,)| {
                let fc = cr
                    .data_mut::<Arc<Mutex<Fancontrold>>>(cx.path())
                    .map(|d| d.clone())
                    .ok_or_else(|| MethodErr::no_path(cx.path()));

                async move {
                    match fc {
                        Ok(fc) => {
                            let mut fc = fc.lock_owned().await;

                            let result = fc
                                .apply_config(config)
                                .await
                                .map_err(|e| MethodErr::failed(&e));
                            let new_config = fc.config().as_string();
                            signal_new_config(&mut cx, new_config);

                            cx.reply(result)
                        }
                        Err(e) => cx.reply(Err(e)),
                    }
                }
            },
        );

        b.method_with_cr_async(
            "Check",
            ("config",),
            ("errors",),
            |mut cx, cr, (config,): (String,)| {
                let fc = cr
                    .data_mut::<Arc<Mutex<Fancontrold>>>(cx.path())
                    .map(|d| d.clone())
                    .ok_or_else(|| MethodErr::no_path(cx.path()));

                async move {
                    match fc {
                        Ok(fc) => {
                            let fc = fc.lock_owned().await;

                            let errors: Vec<String> = fc
                                .check_config(config)
                                .await
                                .iter()
                                .map(|e| e.to_string())
                                .collect();

                            cx.reply(Ok((errors,)))
                        }
                        Err(e) => cx.reply(Err(e)),
                    }
                }
            },
        );
    });

    cr.insert(dbus_object_path(), &[interface], fc);

    conn.start_receive(
        dbus::message::MatchRule::new_method_call(),
        Box::new(move |msg, conn| {
            cr.handle_message(msg, conn).unwrap();
            true
        }),
    );

    Ok(())
}

pub fn new_config_signal(config: impl Into<String>) -> Message {
    Message::new_signal(dbus_object_path(), dbus_interface_name(), "NewConfig")
        .unwrap()
        .append1(config.into())
}

pub fn new_error_signal(error: impl Into<String>) -> Message {
    Message::new_signal(dbus_object_path(), dbus_interface_name(), "Error")
        .unwrap()
        .append1(error.into())
}
