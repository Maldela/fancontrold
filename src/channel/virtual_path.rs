use std::{
    fmt::Debug,
    path::{Path, PathBuf},
};

use libmedium::{sensors::async_sensors::virt::AsyncVirtualSensor, units::Temperature};

use super::{Error, Result};

pub(super) struct VirtualPath {
    virt: Box<dyn AsyncVirtualSensor<Temperature> + Send + Sync>,
    path: PathBuf,
}

impl VirtualPath {
    pub(super) fn new(path: impl Into<PathBuf>) -> Result<Self> {
        let path = path.into();
        let virt =
            match libmedium::sensors::async_sensors::virt::virtual_sensor_from_path(path.clone()) {
                Ok(virt) => Box::new(virt),
                Err(e) => return Err(Error::virtual_temp(path, e)),
            };

        Ok(Self { virt, path })
    }

    pub(super) fn path(&self) -> &Path {
        &self.path
    }

    pub(super) async fn read(&self) -> Result<Temperature> {
        self.virt
            .read()
            .await
            .map_err(|e| Error::virtual_temp(self.path.clone(), e))
    }
}

impl Debug for VirtualPath {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let path_dbg = self.path.fmt(f)?;
        write!(f, "{:?}", path_dbg)
    }
}
