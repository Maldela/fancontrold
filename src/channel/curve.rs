use libmedium::units::{Pwm, Temperature};

use std::collections::BTreeMap;

pub trait Curve {
    /// Returns the point with the lowest temperature.
    /// If the curve is empty, this returns (i32::min_value(), 255).
    fn lowest_point(&self) -> (i32, u8);

    /// Returns the point with the highest temperature.
    /// If the curve is empty, this returns (i32::max_value(), 255).
    fn highest_point(&self) -> (i32, u8);

    /// Returns the point with the next lower temperature than temp.
    /// If the curve is empty, this returns (i32::min_value(), 255).
    fn next_lower_point(&self, temp: Temperature) -> (i32, u8);

    /// Returns the point with the next higher temperature than temp.
    /// If the curve is empty, this returns (i32::max_value(), 255).
    fn next_higher_point(&self, temp: Temperature) -> (i32, u8);

    /// Returns the interpolated pwm value corresponding to temp.
    /// If temp is higher than the highest point on the curve, the pwm value of that point is returned.
    /// If temp is lower than the lowest point on the curve, zero is returned.
    /// Returns 255 if the curve is empty.
    fn value(&self, temp: Temperature) -> Pwm;
}

impl Curve for BTreeMap<i32, u8> {
    fn lowest_point(&self) -> (i32, u8) {
        if self.is_empty() {
            return (i32::min_value(), 255);
        }

        self.iter().map(|(&temp, &pwm)| (temp, pwm)).next().unwrap()
    }

    fn highest_point(&self) -> (i32, u8) {
        if self.is_empty() {
            return (i32::max_value(), 255);
        }

        self.iter().map(|(&temp, &pwm)| (temp, pwm)).last().unwrap()
    }

    fn next_lower_point(&self, temp: Temperature) -> (i32, u8) {
        self.iter()
            .filter(|(&t, _)| t * 1000 <= temp.as_millidegrees_celsius())
            .fold(self.lowest_point(), |point, (&t, &p)| {
                if t > point.0 {
                    (t, p)
                } else {
                    point
                }
            })
    }

    fn next_higher_point(&self, temp: Temperature) -> (i32, u8) {
        self.iter()
            .filter(|(&t, _)| t * 1000 >= temp.as_millidegrees_celsius())
            .fold(self.highest_point(), |point, (&t, &p)| {
                if t < point.0 {
                    (t, p)
                } else {
                    point
                }
            })
    }

    fn value(&self, temp: Temperature) -> Pwm {
        if self.is_empty() {
            return Pwm::from_u8(255);
        }

        if temp.as_millidegrees_celsius() < self.lowest_point().0 * 1000 {
            return Pwm::from_u8(0);
        }

        let highest_point = self.highest_point();
        if temp.as_millidegrees_celsius() > highest_point.0 * 1000 {
            return Pwm::from_u8(highest_point.1);
        }

        let lower_point = self.next_lower_point(temp);
        let upper_point = self.next_higher_point(temp);
        let delta_temp = upper_point.0 - lower_point.0;
        if delta_temp == 0 {
            return Pwm::from_u8(upper_point.1);
        }
        let delta_pwm = f64::from(upper_point.1) - f64::from(lower_point.1);
        let slope = delta_pwm / f64::from(delta_temp);
        let distance = temp.as_degrees_celsius() - f64::from(lower_point.0);
        Pwm::from_u8(((distance * slope).round() + f64::from(lower_point.1)) as u8)
    }
}
