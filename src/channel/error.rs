use std::{
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    path::PathBuf,
};

use libfancontrold::AddressError;

use crate::Address;

use libmedium::sensors::Error as SensorError;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    NoValidTemps,
    Average,
    EmptyCurve,
    Rpm {
        source: SensorError,
    },
    Pwm {
        source: SensorError,
    },
    PwmEnable {
        source: SensorError,
    },
    PwmMode {
        source: SensorError,
    },
    PwmNotWritable {
        address: Address,
    },
    Temp {
        address: Address,
        source: SensorError,
    },
    VirtualTemp {
        path: PathBuf,
        source: SensorError,
    },
    SensorNotFound {
        source: AddressError,
    },
    Compound {
        sources: Vec<Error>,
    },
}

impl Error {
    pub fn temp(address: impl Into<Address>, source: SensorError) -> Self {
        Error::Temp {
            address: address.into(),
            source,
        }
    }

    pub fn virtual_temp(path: impl Into<PathBuf>, source: SensorError) -> Self {
        Error::VirtualTemp {
            path: path.into(),
            source,
        }
    }

    pub fn pwm(source: SensorError) -> Self {
        Error::Pwm { source }
    }

    pub fn pwm_enable(source: SensorError) -> Self {
        Error::PwmEnable { source }
    }

    pub fn pwm_mode(source: SensorError) -> Self {
        Error::PwmMode { source }
    }

    pub fn pwm_not_writable(address: Address) -> Self {
        Error::PwmNotWritable { address }
    }

    pub fn rpm(source: SensorError) -> Self {
        Error::Rpm { source }
    }

    pub fn compound(sources: impl Into<Vec<Error>>) -> Option<Self> {
        let mut sources = sources.into();

        match sources.len() {
            0 => return None,
            1 => return Some(sources.pop().unwrap()),
            _ => return Some(Error::Compound { sources }),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Error::NoValidTemps => write!(f, "Channel has no valid temps."),
            Error::Average => write!(f, "Channel has an average below one."),
            Error::EmptyCurve => write!(f, "Channel has an empty curve."),
            Error::Temp { address, source } => {
                write!(f, "Error reading temp sensor at {}: {}", address, source)
            }
            Error::VirtualTemp { path, source } => {
                write!(
                    f,
                    "Error reading virtual temp sensor at {}: {}",
                    path.display(),
                    source
                )
            }
            Error::Rpm { source } => write!(f, "Error reading rpm sensor: {}.", source),
            Error::Pwm { source } => write!(f, "Error setting pwm value: {}.", source),
            Error::PwmEnable { source } => write!(f, "Error setting pwm_enable value: {}.", source),
            Error::PwmMode { source } => write!(f, "Error setting pwm_mode value: {}.", source),
            Error::PwmNotWritable { address } => {
                write!(f, "Pwm sensor at {} is not writable.", address)
            }
            Error::SensorNotFound { source } => write!(f, "Sensor not found: {}.", source),
            Error::Compound { sources } => match sources.len() {
                0 => Ok(()),
                1 => sources.first().unwrap().fmt(f),
                _ => {
                    writeln!(f, "Multiple errors:")?;
                    for source in sources {
                        source.fmt(f)?;
                        writeln!(f)?;
                    }
                    Ok(())
                }
            },
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            Error::NoValidTemps => None,
            Error::Average => None,
            Error::EmptyCurve => None,
            Error::Temp { source, .. } => Some(source),
            Error::VirtualTemp { source, .. } => Some(source),
            Error::Rpm { source } => Some(source),
            Error::Pwm { source } => Some(source),
            Error::PwmEnable { source } => Some(source),
            Error::PwmMode { source } => Some(source),
            Error::PwmNotWritable { .. } => None,
            Error::SensorNotFound { source } => Some(source),
            Error::Compound { sources } => sources.first().map(|s| {
                let s: &dyn StdError = s;
                s
            }),
        }
    }
}

impl From<AddressError> for Error {
    fn from(source: AddressError) -> Self {
        Error::SensorNotFound { source }
    }
}
