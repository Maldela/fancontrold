use crate::{channel::Error as ChannelError, Address};

use libfancontrold::Error as LibError;

use std::{
    convert::Infallible,
    error::Error as StdError,
    fmt::{Display, Formatter, Result as FmtResult},
    path::PathBuf,
    result::Result as StdResult,
};

use libmedium::ParsingError;

pub type Result<T> = StdResult<T, Error>;

/// The different error types Error can be.
#[derive(Debug)]
pub enum Error {
    InvalidConfigPath {
        path: PathBuf,
    },
    Lib {
        source: LibError,
    },
    Parsing {
        source: ParsingError,
    },
    ChannelCreation {
        address: Address,
        source: ChannelError,
    },
    ChannelUpdate {
        address: Address,
        source: ChannelError,
    },
    ChannelReset {
        address: Address,
        source: ChannelError,
    },
    NoPwms,
    Compound {
        sources: Vec<Error>,
    },
}

impl Error {
    pub(crate) fn channel_creation(source: ChannelError, address: impl Into<Address>) -> Self {
        Error::ChannelCreation {
            source,
            address: address.into(),
        }
    }

    pub(crate) fn channel_update(source: ChannelError, address: impl Into<Address>) -> Self {
        Error::ChannelUpdate {
            source,
            address: address.into(),
        }
    }

    pub(crate) fn channel_reset(source: ChannelError, address: impl Into<Address>) -> Self {
        Error::ChannelReset {
            source,
            address: address.into(),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> FmtResult {
        match self {
            Error::InvalidConfigPath { path } => {
                write!(f, "Invalid config path {}", path.display())
            }
            Error::Lib { source } => write!(f, "Libfancontrold error: {}", source),
            Error::Parsing { source } => write!(f, "Libmedium parsing error: {}", source),
            Error::ChannelCreation { address, source } => {
                write!(f, "Error creating channel for pwm {}: {}", address, source)
            }
            Error::ChannelUpdate { address, source } => {
                write!(f, "Error updating channel {}: {}", address, source)
            }
            Error::ChannelReset { address, source } => {
                write!(f, "Error resetting channel {}: {}", address, source)
            }
            Error::NoPwms => write!(f, "System does not have any pwm capable fans."),
            Error::Compound { sources } => match sources.len() {
                0 => Ok(()),
                1 => sources.first().unwrap().fmt(f),
                _ => {
                    writeln!(f, "Multiple errors:")?;
                    for source in sources {
                        source.fmt(f)?;
                        writeln!(f)?;
                    }
                    Ok(())
                }
            },
        }
    }
}

impl StdError for Error {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            Error::InvalidConfigPath { .. } => None,
            Error::Lib { source } => Some(source),
            Error::Parsing { source } => Some(source),
            Error::ChannelCreation { source, .. } => Some(source),
            Error::ChannelUpdate { source, .. } => Some(source),
            Error::ChannelReset { source, .. } => Some(source),
            Error::NoPwms => None,
            Error::Compound { sources } => sources.first().map(|s| {
                let s: &dyn StdError = s;
                s
            }),
        }
    }
}

impl From<libfancontrold::Error> for Error {
    fn from(source: libfancontrold::Error) -> Self {
        Error::Lib { source }
    }
}

impl From<libfancontrold::config::Error> for Error {
    fn from(source: libfancontrold::config::Error) -> Self {
        let source = libfancontrold::Error::Config { source };
        Error::Lib { source }
    }
}

impl From<ParsingError> for Error {
    fn from(source: ParsingError) -> Self {
        Error::Parsing { source }
    }
}

impl From<Infallible> for Error {
    fn from(_: Infallible) -> Self {
        unreachable!()
    }
}

pub trait IntoResult<T> {
    fn into_result(self) -> Result<T>;
}

impl IntoResult<()> for Vec<Error> {
    fn into_result(mut self) -> Result<()> {
        match self.len() {
            0 => Ok(()),
            1 => Err(self.pop().unwrap()),
            _ => Err(Error::Compound { sources: self }),
        }
    }
}
