mod curve;
mod error;
mod virtual_path;

use curve::Curve;
pub use error::{Error, Result};
use virtual_path::VirtualPath;

use super::Address;

use std::{
    cmp,
    collections::{BTreeMap, HashMap},
    fmt::Debug,
};

use libmedium::{
    sensors::{
        async_sensors::{fan::AsyncFanSensor, pwm::AsyncWriteablePwmSensor, temp::AsyncTempSensor},
        Error as SensorError, SensorSubFunctionType,
    },
    units::{Pwm, PwmEnable, PwmMode, Temperature},
};

use libfancontrold::{
    config::{ChannelConfig, MultiTempFunction},
    Addressable,
};

use futures::StreamExt;

pub type Channels = HashMap<Address, Channel>;

fn average_temp(temps: Vec<Temperature>) -> Option<Temperature> {
    if temps.is_empty() {
        return None;
    }

    let len = temps.len() as i32;
    let sum = temps
        .into_iter()
        .fold(0, |sum, temp| sum + temp.as_millidegrees_celsius());

    Some(Temperature::from_millidegrees_celsius(sum / len))
}

#[derive(Debug)]
pub struct Channel {
    fan_sensor: Box<dyn AsyncFanSensor + Send + Sync>,
    pwm_sensor: Box<dyn AsyncWriteablePwmSensor + Send + Sync>,
    temps: HashMap<Address, Box<dyn AsyncTempSensor + Send + Sync>>,
    virtual_temps: Vec<VirtualPath>,
    pwm_enable_restore: Option<PwmEnable>,
    temp_last: Temperature,
    mtf: MultiTempFunction,
    min_start: Pwm,
    curve: BTreeMap<i32, u8>,
    pwm_mode: PwmMode,
    min_pwm: Pwm,
    average: u8,
}

impl Channel {
    pub async fn new(hwmons: &impl Addressable, config: &ChannelConfig) -> Result<Self> {
        let pwm_sensor = hwmons.get_pwm(&config.address())?;

        if !pwm_sensor
            .supported_write_sub_functions()
            .contains(&SensorSubFunctionType::Pwm)
        {
            return Err(Error::pwm_not_writable(config.address()));
        }

        let pwm_enable = match pwm_sensor.read_enable().await {
            Ok(pe) => Some(pe),
            Err(e) => match e {
                SensorError::SubtypeNotSupported { .. } => None,
                _ => return Err(Error::pwm_enable(e)),
            },
        };

        Ok(Channel {
            fan_sensor: hwmons.get_fan(&config.address())?,
            pwm_sensor,
            temps: config
                .temps
                .iter()
                .try_fold(HashMap::new(), |mut temps, address| {
                    let temp = match hwmons.get_temp(address) {
                        Ok(temp) => temp,
                        Err(e) => return Err(e),
                    };
                    temps.insert(address.clone(), temp);
                    Ok(temps)
                })?,
            virtual_temps: config.virtual_temps.iter().try_fold(
                Vec::new(),
                |mut virt_temps, path| -> Result<Vec<VirtualPath>> {
                    let virt_temp = VirtualPath::new(path.clone())?;
                    virt_temps.push(virt_temp);
                    Ok(virt_temps)
                },
            )?,
            pwm_enable_restore: pwm_enable,
            temp_last: Temperature::from_millidegrees_celsius(0),
            mtf: config.mtf,
            min_start: Pwm::from_u8(config.min_start),
            curve: config.curve.clone(),
            pwm_mode: config.pwm_mode,
            min_pwm: Pwm::from_u8(config.min_pwm),
            average: config.average,
        })
    }

    pub async fn update(&mut self) -> Result<Pwm> {
        let mut pwm: Pwm;

        let temps_stream = futures::stream::iter(&self.temps).then(|(a, t)| async {
            t.read_input()
                .await
                .map_err(|e| Error::temp(a.to_owned(), e))
        });

        let virtual_temps_stream =
            futures::stream::iter(&self.virtual_temps).then(|vt| async { vt.read().await });

        let (temps, errors) = temps_stream
            .chain(virtual_temps_stream)
            .fold(
                (Vec::with_capacity(self.temps.len()), Vec::new()),
                |(mut temps, mut errors), r| async {
                    match r {
                        Ok(t) => temps.push(t),
                        Err(e) => errors.push(e),
                    };
                    (temps, errors)
                },
            )
            .await;

        if let Some(te) = Error::compound(errors) {
            if let Err(pe) = self.pwm_sensor.write_pwm(Pwm::FULLSPEED).await {
                return Err(Error::compound(vec![te, Error::pwm(pe)]).unwrap());
            }

            return Err(te);
        }

        if temps.is_empty() {
            return Err(Error::NoValidTemps);
        }

        let temp = match self.mtf {
            MultiTempFunction::Min => temps.into_iter().min().unwrap(),
            MultiTempFunction::Max => temps.into_iter().max().unwrap(),
            MultiTempFunction::Average => average_temp(temps).unwrap(),
        };

        if self.average < 1 {
            return Err(Error::Average);
        }
        let averaged_temp =
            (self.temp_last * (i32::from(self.average) - 1) + temp) / i32::from(self.average);

        let rpm = self.fan_sensor.read_input().await.map_err(Error::rpm)?;

        if self.curve.is_empty() {
            return Err(Error::EmptyCurve);
        } else {
            pwm = self.curve.value(averaged_temp);
        }

        self.temp_last = averaged_temp;

        // Apply min_pwm limit
        pwm = cmp::max(pwm, self.min_pwm);

        //start pwm if it stopped and should not have
        if rpm.as_rpm() == 0 && self.min_pwm > Pwm::from_u8(0) {
            pwm = cmp::max(pwm, self.min_start);
        }

        if self.has_enable_function() {
            self.pwm_sensor
                .write_enable(PwmEnable::ManualControl)
                .await
                .map_err(Error::pwm_enable)?;
        }

        self.pwm_sensor.write_pwm(pwm).await.map_err(Error::pwm)?;

        if self.has_mode_function() {
            // For true fan off switch to DC mode
            let pwm_mode = if pwm == Pwm::from_u8(0) {
                PwmMode::Dc
            } else {
                self.pwm_mode
            };

            self.pwm_sensor
                .write_mode(pwm_mode)
                .await
                .map_err(Error::pwm_mode)?;
        }

        Ok(pwm)
    }

    pub fn has_enable_function(&self) -> bool {
        self.pwm_sensor
            .supported_read_sub_functions()
            .contains(&SensorSubFunctionType::Enable)
    }

    pub fn has_mode_function(&self) -> bool {
        self.pwm_sensor
            .supported_read_sub_functions()
            .contains(&SensorSubFunctionType::Mode)
    }

    pub async fn restore_enable(&mut self) -> Result<()> {
        if self.has_enable_function() && self.pwm_enable_restore.is_some() {
            self.pwm_sensor
                .write_enable(self.pwm_enable_restore.unwrap())
                .await
                .map_err(Error::pwm_enable)?;
        }

        Ok(())
    }

    pub fn config(&self) -> ChannelConfig {
        let mut config = ChannelConfig {
            hwmon_device_path: self
                .pwm_sensor
                .hwmon_path()
                .join("device")
                .canonicalize()
                .unwrap(),
            index: self.pwm_sensor.index(),
            curve: self.curve.clone(),
            temps: self.temps.keys().map(Clone::clone).collect(),
            virtual_temps: self
                .virtual_temps
                .iter()
                .map(|s| s.path().to_path_buf())
                .collect(),
            mtf: self.mtf,
            min_pwm: self.min_pwm.as_u8(),
            min_start: self.min_start.as_u8(),
            pwm_mode: self.pwm_mode,
            average: self.average,
        };

        config.sort();

        config
    }
}

#[cfg(test)]
#[allow(unused_must_use)]
mod tests {
    use super::*;
    use crate::tests::*;

    use libmedium::hwmon::async_hwmon::Hwmons;

    use std::collections::BTreeMap;
    use std::fs::{remove_dir_all, write};
    use std::path::Path;

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn multiple_sensors_test() {
        let test_path = Path::new("multiple_sensors_test");

        remove_dir_all(test_path);

        VirtualHwmonBuilder::create(test_path, 0, "system")
            .add_pwm(1, true, true)
            .add_temp(1, 40000, "temp1")
            .add_temp(2, 60000, "temp2");

        let hwmons = Hwmons::parse_unrestricted(test_path).await.unwrap();
        let hwmon = hwmons.hwmon_by_index(0).unwrap();
        let temps = hwmon
            .temps()
            .iter()
            .map(|(&i, _)| Address::new(hwmon.device_path(), i))
            .collect();

        let mut curve = BTreeMap::new();
        curve.insert(0, 0);
        curve.insert(100, 100);

        let mut config = ChannelConfig::new(
            test_path.canonicalize().unwrap(),
            1,
            curve,
            temps,
            Vec::new(),
        );
        config.min_start = 0;
        config.min_pwm = 0;
        config.mtf = MultiTempFunction::Min;

        let mut channel = Channel::new(&hwmons, &config).await.unwrap();

        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(40), updated_pwm);

        channel.mtf = MultiTempFunction::Max;
        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(60), updated_pwm);

        channel.mtf = MultiTempFunction::Average;
        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(50), updated_pwm);

        remove_dir_all(test_path).unwrap();
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn below_curve_test() {
        let test_path = Path::new("fan_below_curve_test");

        remove_dir_all(test_path);

        VirtualHwmonBuilder::create(test_path, 0, "system")
            .add_pwm(1, true, true)
            .add_temp(1, 40000, "temp1");

        let hwmons = Hwmons::parse_unrestricted(test_path).await.unwrap();
        let hwmon = hwmons.hwmon_by_index(0).unwrap();
        let temps = hwmon
            .temps()
            .iter()
            .map(|(&i, _)| Address::new(hwmon.device_path(), i))
            .collect();

        let mut curve = BTreeMap::new();
        curve.insert(100, 100);
        curve.insert(90, 90);
        curve.insert(110, 200);

        let mut config = ChannelConfig::new(
            test_path.canonicalize().unwrap(),
            1,
            curve,
            temps,
            Vec::new(),
        );
        config.min_start = 0;
        config.min_pwm = 80;

        let mut channel = Channel::new(&hwmons, &config).await.unwrap();

        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(80), updated_pwm);

        channel.min_pwm = Pwm::from_u8(0);
        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(0), updated_pwm);

        let fan_path = test_path.join("hwmon0").join("fan1_input");
        write(fan_path, "0").unwrap();

        channel.min_pwm = Pwm::from_u8(80);
        channel.min_start = Pwm::from_u8(200);

        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(200), updated_pwm);

        remove_dir_all(test_path).unwrap();
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn above_curve_test() {
        let test_path = Path::new("fan_above_curve_test");

        remove_dir_all(test_path);

        VirtualHwmonBuilder::create(test_path, 0, "system")
            .add_pwm(1, true, true)
            .add_temp(1, 40000, "temp1");

        let hwmons = Hwmons::parse_unrestricted(test_path).await.unwrap();
        let hwmon = hwmons.hwmon_by_index(0).unwrap();
        let temps = hwmon
            .temps()
            .iter()
            .map(|(&i, _)| Address::new(hwmon.device_path(), i))
            .collect();

        let mut curve = BTreeMap::new();
        curve.insert(0, 100);
        curve.insert(10, 200);

        let mut config = ChannelConfig::new(
            test_path.canonicalize().unwrap(),
            1,
            curve,
            temps,
            Vec::new(),
        );
        config.min_start = 0;

        let mut channel = Channel::new(&hwmons, &config).await.unwrap();

        let updated_pwm = channel.update().await.unwrap();

        assert_eq!(Pwm::from_u8(200), updated_pwm);

        remove_dir_all(test_path).unwrap();
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn empty_curve_test() {
        let test_path = Path::new("fan_empty_curve_test");

        remove_dir_all(test_path);

        VirtualHwmonBuilder::create(test_path, 0, "system")
            .add_pwm(1, true, true)
            .add_temp(1, 40000, "temp1");

        let hwmons = Hwmons::parse_unrestricted(test_path).await.unwrap();
        let hwmon = hwmons.hwmon_by_index(0).unwrap();
        let temps = hwmon
            .temps()
            .iter()
            .map(|(&i, _)| Address::new(hwmon.device_path(), i))
            .collect();

        let config = ChannelConfig::new(
            test_path.canonicalize().unwrap(),
            1,
            BTreeMap::new(),
            temps,
            Vec::new(),
        );

        let mut channel = Channel::new(&hwmons, &config).await.unwrap();

        channel.update().await.unwrap_err();

        remove_dir_all(test_path).unwrap();
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn average_test() {
        let test_path = Path::new("average_test");

        remove_dir_all(test_path);

        VirtualHwmonBuilder::create(test_path, 0, "system")
            .add_pwm(1, true, true)
            .add_temp(1, 50000, "temp1")
            .add_temp(2, 20000, "temp2");

        let hwmons = Hwmons::parse_unrestricted(test_path).await.unwrap();
        let hwmon = hwmons.hwmon_by_index(0).unwrap();
        let temps = hwmon.temps();

        let temp1: Box<dyn AsyncTempSensor + Send + Sync> =
            Box::new(temps.get(&1).unwrap().clone());
        let mut temps1 = HashMap::new();
        temps1.insert(Address::new(hwmon.device_path(), 1), temp1);

        let mut curve = BTreeMap::new();
        curve.insert(0, 0);
        curve.insert(100, 100);

        let mut config = ChannelConfig::new(
            test_path.canonicalize().unwrap(),
            1,
            curve,
            Vec::new(),
            Vec::new(),
        );
        config.min_start = 0;
        config.min_pwm = 0;
        config.average = 1;

        let mut channel = Channel::new(&hwmons, &config).await.unwrap();
        channel.temps = temps1;

        let updated_pwm = channel.update().await.unwrap();
        assert_eq!(updated_pwm, Pwm::from_u8(50));

        let temp2: Box<dyn AsyncTempSensor + Send + Sync> =
            Box::new(temps.get(&2).unwrap().clone());
        let mut temps2 = HashMap::new();
        temps2.insert(Address::new(hwmon.device_path(), 2), temp2);

        channel.average = 5;
        channel.temps = temps2;

        let updated_pwm = channel.update().await.unwrap();
        assert_eq!(updated_pwm, Pwm::from_u8(44));

        let updated_pwm = channel.update().await.unwrap();
        assert_eq!(updated_pwm, Pwm::from_u8(39));

        let updated_pwm = channel.update().await.unwrap();
        assert_eq!(updated_pwm, Pwm::from_u8(35));

        remove_dir_all(test_path).unwrap();
    }
}
