mod channel;
pub mod dbus;
mod error;

use std::{
    collections::HashMap,
    convert::TryInto,
    fmt::Debug,
    path::{Path, PathBuf},
};

use channel::{Channel, Channels};
use error::{Error, IntoResult, Result};

use futures::stream::StreamExt;

use libfancontrold::{
    config::{ChannelConfig, Config},
    Address,
};

use log::*;

#[derive(Debug)]
pub struct Fancontrold {
    hwmons: libmedium::hwmon::async_hwmon::Hwmons,
    channels: Channels,
    config_path: PathBuf,
}

impl Fancontrold {
    pub async fn new() -> Result<Self> {
        Ok(Fancontrold {
            hwmons: libmedium::parse_hwmons_async().await?,
            channels: HashMap::new(),
            config_path: PathBuf::new(),
        })
    }

    /// Checks given `Config` for errors and incompatibilities with the current system.
    pub async fn check_config<T>(&self, config: T) -> Vec<Error>
    where
        T: TryInto<Config>,
        Error: From<<T as TryInto<Config>>::Error>,
        libfancontrold::Error: From<<T as TryInto<Config>>::Error>,
    {
        libfancontrold::check_config(config, &self.hwmons)
            .await
            .into_iter()
            .map(Into::into)
            .collect()
    }

    /// Applies the given `Config`.
    pub async fn apply_config<T>(&mut self, config: T) -> Result<()>
    where
        T: TryInto<Config>,
        Error: From<<T as TryInto<Config>>::Error>,
    {
        // reset all channels
        self.reset().await?;

        let config = config.try_into()?;

        debug!("Applying config: {:#?}", &config);

        let mut errors = Vec::new();

        for channel_config in config.channel_configs() {
            let channel_address = channel_config.address();

            match Channel::new(&self.hwmons, channel_config).await {
                Ok(channel) => {
                    self.channels.insert(channel_address, channel);
                }
                Err(e) => {
                    errors.push(Error::channel_creation(e, channel_address));
                }
            }
        }

        if !errors.is_empty() {
            warn!("One or more invalid channels in config.");
        }

        errors.into_result()
    }

    pub fn config(&self) -> Config {
        let mut config: Config = self
            .channels
            .values()
            .map(Channel::config)
            .collect::<Vec<ChannelConfig>>()
            .into();

        config.sort();

        config
    }

    pub fn config_path(&self) -> &Path {
        &self.config_path
    }

    /// Loads the config file at config_path and applies it.
    pub async fn load_config(&mut self, config_path: impl Into<PathBuf>) -> Result<()> {
        let config_path = config_path.into();
        self.config_path = config_path.clone();

        info!("Loading config at {}", config_path.display());

        // parse config
        let config = Config::read(config_path).await?;

        self.apply_config(config).await
    }

    /// Reloads the loaded config.
    /// If no config has been loaded, this returns an error.
    pub async fn reload_config(&mut self) -> Result<()> {
        info!("Reloading config");

        if self.config_path.to_string_lossy().is_empty() {
            return Err(Error::InvalidConfigPath {
                path: PathBuf::new(),
            });
        }

        let config_path = self.config_path.clone();
        self.load_config(config_path).await
    }

    /// Updates all `Channel`s configured by the loaded config.
    /// If no config has been loaded, this does nothing.
    pub async fn update(&mut self) -> Result<()> {
        futures::stream::iter(&mut self.channels)
            .filter_map(|(a, c)| async {
                c.update()
                    .await
                    .err()
                    .map(|e| Error::channel_update(e, a.to_owned()))
            })
            .collect::<Vec<_>>()
            .await
            .into_result()
    }

    /// Deletes all `Channel`s owned by this `Fancontrol`.
    pub async fn reset(&mut self) -> Result<()> {
        let errors: Vec<_> = futures::stream::iter(&mut self.channels)
            .filter_map(|(a, c)| async {
                c.restore_enable()
                    .await
                    .err()
                    .map(|e| Error::channel_reset(e, a.to_owned()))
            })
            .collect()
            .await;

        self.channels.clear();

        errors.into_result()
    }
}

#[cfg(test)]
mod tests {
    use std::fs;
    use std::fs::{File, OpenOptions};
    use std::io::Write;
    use std::path::{Path, PathBuf};

    pub struct VirtualHwmonBuilder {
        path: PathBuf,
    }

    impl VirtualHwmonBuilder {
        pub fn create(
            path: impl AsRef<Path>,
            index: u16,
            name: impl AsRef<[u8]>,
        ) -> VirtualHwmonBuilder {
            let path = path.as_ref().join(format!("hwmon{}", index));

            fs::create_dir_all(&path).unwrap();

            File::create(path.join("name"))
                .unwrap()
                .write(name.as_ref())
                .unwrap();

            std::os::unix::fs::symlink(
                path.parent().unwrap().canonicalize().unwrap(),
                path.join("device"),
            )
            .unwrap();

            VirtualHwmonBuilder { path }
        }

        pub fn add_temp(
            self,
            index: u16,
            value: i32,
            label: impl AsRef<str>,
        ) -> VirtualHwmonBuilder {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .truncate(true)
                .open(self.path().join(format!("temp{}_input", index)))
                .unwrap()
                .write(value.to_string().as_bytes())
                .unwrap();

            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .truncate(true)
                .open(self.path().join(format!("temp{}_label", index)))
                .unwrap()
                .write(label.as_ref().as_bytes())
                .unwrap();

            self
        }

        pub fn add_fan(self, index: u16, value: u32) -> VirtualHwmonBuilder {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .truncate(true)
                .open(self.path().join(format!("fan{}_input", index)))
                .unwrap()
                .write(value.to_string().as_bytes())
                .unwrap();

            self
        }

        pub fn add_pwm(
            self,
            index: u16,
            create_enable_file: bool,
            create_mode_file: bool,
        ) -> VirtualHwmonBuilder {
            OpenOptions::new()
                .read(true)
                .write(true)
                .create(true)
                .truncate(true)
                .open(self.path.join(&format!("pwm{}", index)))
                .unwrap()
                .write(b"0")
                .unwrap();
            if create_enable_file {
                OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(self.path.join(&format!("pwm{}_enable", index)))
                    .unwrap()
                    .write(b"2")
                    .unwrap();
            }
            if create_mode_file {
                OpenOptions::new()
                    .read(true)
                    .write(true)
                    .create(true)
                    .truncate(true)
                    .open(self.path.join(&format!("pwm{}_mode", index)))
                    .unwrap()
                    .write(b"1")
                    .unwrap();
            }

            self.add_fan(index, 1000)
        }

        pub fn path(&self) -> &Path {
            self.path.as_path()
        }
    }
}
